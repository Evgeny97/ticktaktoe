package ru.nsu.belov.ticktaktoe.authentication;

/**
 * Created by Evgeny on 15.12.2017.
 */

public interface CreateUserCompleteListener {
    void onCreateUserSuccess(User user);
    void onCreateUserFailure(Throwable exception);
}
