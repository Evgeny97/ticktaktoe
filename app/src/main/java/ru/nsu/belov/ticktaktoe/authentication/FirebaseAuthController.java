package ru.nsu.belov.ticktaktoe.authentication;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.GetTokenResult;

/**
 * Created by Evgeny on 15.12.2017.
 */

public class FirebaseAuthController implements AuthenticationController {
    private FirebaseAuth mAuth;

    private SignInCompleteListener signInCompleteListener;
    private CreateUserCompleteListener createUserCompleteListener;

    public FirebaseAuthController() {
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void createNewUser(String login, String password, final CreateUserCompleteListener createUserCompleteListener) {
        this.createUserCompleteListener = createUserCompleteListener;
        mAuth.createUserWithEmailAndPassword(login, password).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                successCreateUser(authResult);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                failureCreateUser(e);
            }
        });
    }

    @Override
    public void signInWithLoginAndPassword(String login, String password, final SignInCompleteListener signInCompleteListener) {
        this.signInCompleteListener = signInCompleteListener;
        mAuth.signInWithEmailAndPassword(login, password).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                successSignIn(authResult);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                failureSignIn(e);
            }
        });
    }

    private void successSignIn(final AuthResult authResult) {
        authResult.getUser().getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
            @Override
            public void onComplete(@NonNull Task<GetTokenResult> task) {
                String token = task.getResult().getToken();
                String email = authResult.getUser().getEmail();
                signInCompleteListener.onSignInSuccess(new User(email, token));
            }
        });
    }

    private void successCreateUser(final AuthResult authResult) {
        String token = authResult.getUser().getUid();
        String email = authResult.getUser().getEmail();
        createUserCompleteListener.onCreateUserSuccess(new User(email, token));
//        authResult.getUser().getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
//            @Override
//            public void onComplete(@NonNull Task<GetTokenResult> task) {
//
//            }
//        });
    }

    private void failureSignIn(Exception e) {
        if (e instanceof FirebaseAuthInvalidCredentialsException) {
            signInCompleteListener.onSignInFailure(new WrongPasswordException(e.getMessage()));
        } else {
            signInCompleteListener.onSignInFailure(e);
        }
    }

    private void failureCreateUser(Exception e) {
        createUserCompleteListener.onCreateUserFailure(e);
    }
}
