package ru.nsu.belov.ticktaktoe.authentication;

/**
 * Created by Evgeny on 15.12.2017.
 */

public class WrongPasswordException extends Exception {
    public WrongPasswordException() {
    }

    public WrongPasswordException(String message) {
        super(message);
    }
}
