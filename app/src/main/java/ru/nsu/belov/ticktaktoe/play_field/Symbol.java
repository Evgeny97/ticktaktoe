package ru.nsu.belov.ticktaktoe.play_field;

/**
 * Created by Evgeny on 13.12.2017.
 */

public enum Symbol {
    NOTHING, CROSS, CIRCLE
}
