package ru.nsu.belov.ticktaktoe;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;

import ru.nsu.belov.ticktaktoe.MainActivity;
import ru.nsu.belov.ticktaktoe.R;
import ru.nsu.belov.ticktaktoe.authentication.AuthenticationController;
import ru.nsu.belov.ticktaktoe.authentication.CreateUserCompleteListener;
import ru.nsu.belov.ticktaktoe.authentication.FirebaseAuthController;
import ru.nsu.belov.ticktaktoe.authentication.SignInCompleteListener;
import ru.nsu.belov.ticktaktoe.authentication.User;
import ru.nsu.belov.ticktaktoe.authentication.WrongPasswordException;

public class LoginActivity extends AppCompatActivity implements SignInCompleteListener, CreateUserCompleteListener {

    private static final String TAG = "Login Activity";
    AuthenticationController authenticationController;
    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mEmailView = findViewById(R.id.email);
        mPasswordView = findViewById(R.id.password);
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);


        Button signInButton = findViewById(R.id.email_sign_in_button);
        signInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });

        Button registrationButton = findViewById(R.id.email_registration_button);
        registrationButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                createUser();
            }
        });
        authenticationController = new FirebaseAuthController();
        if(FirebaseAuth.getInstance().getCurrentUser()!=null){
            goToMainActivity(null);
        }
    }

    private boolean isDataValid() {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);
        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }
        if (cancel) {
            focusView.requestFocus();
        }
        return !cancel;
    }

    private void signIn() {
        showProgress(true);
        if(isDataValid()){
            String email = mEmailView.getText().toString();
            String password = mPasswordView.getText().toString();
            authenticationController.signInWithLoginAndPassword(email,password,this);
        }else {
            showProgress(false);
        }
    }

    private void createUser() {
        showProgress(true);
        if(isDataValid()){
            String email = mEmailView.getText().toString();
            String password = mPasswordView.getText().toString();
            authenticationController.createNewUser(email,password,this);
        }else {
            showProgress(false);
        }
    }

    private void goToMainActivity(User user) {
        startActivity(MainActivity.getCreateIntent(this, user));
        finish();
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return !TextUtils.isEmpty(password) && password.length() > 5;
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void onSignInSuccess(User user) {
        this.user = user;
        goToMainActivity(user);
    }

    @Override
    public void onSignInFailure(Throwable exception) {
        showProgress(false);
        if(exception instanceof WrongPasswordException){
            mPasswordView.setError(exception.getMessage());
            mPasswordView.requestFocus();
        }
        else {
            mEmailView.setError(exception.getMessage());
            mEmailView.requestFocus();
        }
    }

    @Override
    public void onCreateUserSuccess(User user) {
        this.user = user;
        goToMainActivity(user);
    }

    @Override
    public void onCreateUserFailure(Throwable exception) {
        showProgress(false);
        mEmailView.setError(exception.getMessage());
        mEmailView.requestFocus();
    }
}

