package ru.nsu.belov.ticktaktoe.play_field;

import android.util.Pair;

/**
 * Created by Evgeny on 13.12.2017.
 */

public interface OnFieldTapListener {
     void onFieldTap(int x , int y);
}
