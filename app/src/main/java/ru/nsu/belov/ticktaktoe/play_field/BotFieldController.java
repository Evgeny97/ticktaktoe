package ru.nsu.belov.ticktaktoe.play_field;

import android.widget.Toast;

import java.util.Random;

import ru.nsu.belov.ticktaktoe.PlayFieldActivity;

/**
 * Created by Evgeny on 20.12.2017.
 */

public class BotFieldController implements OnFieldTapListener {

    private final Symbol botSymbol;
    private PlayFieldView playFieldView;
    private PlayFieldActivity playFieldActivity;
    private Symbol currTurn;
    private Symbol mySymbol;
    private boolean gameEnded = false;
    private Bot bot;
    private int turns;

    public BotFieldController(PlayFieldView playFieldView, PlayFieldActivity playFieldActivity) {
        this.playFieldView = playFieldView;
        this.playFieldActivity = playFieldActivity;

        Random random = new Random();
        if (random.nextBoolean()) {
            mySymbol = Symbol.CROSS;
        } else {
            mySymbol = Symbol.CIRCLE;
        }
        botSymbol = (mySymbol == Symbol.CIRCLE) ? Symbol.CROSS : Symbol.CIRCLE;
        bot = new Bot(botSymbol);
        playFieldActivity.setYourSymbolTextView(mySymbol);
        playFieldActivity.setProgressBarVisible(false);
        if (botSymbol == Symbol.CROSS) {
            int[] move = bot.move(playFieldView.matrix);
            playFieldView.setCell(move[1], move[0], botSymbol);
            currTurn = Symbol.CIRCLE;
            turns = 1;
        } else {
            currTurn = Symbol.CROSS;
            turns = 0;
        }
        playFieldActivity.setCurrentTurn(currTurn);
    }

    private void checkToWin(int x, int y) {
        if (playFieldView.getCell(0, y) == playFieldView.getCell(1, y) &&
                playFieldView.getCell(0, y) == playFieldView.getCell(2, y)) {
            playFieldActivity.win(playFieldView.getCell(0, y));
            gameEnded = true;
            return;
        }
        if (playFieldView.getCell(x, 0) == playFieldView.getCell(x, 1) &&
                playFieldView.getCell(x, 0) == playFieldView.getCell(x, 2)) {
            playFieldActivity.win(playFieldView.getCell(x, 0));
            gameEnded = true;
            return;
        }
        if (x == y) {
            if (playFieldView.getCell(0, 0) == playFieldView.getCell(1, 1) &&
                    playFieldView.getCell(0, 0) == playFieldView.getCell(2, 2)) {
                playFieldActivity.win(playFieldView.getCell(0, 0));
                gameEnded = true;
            }
        } else if (x + y == 2) {
            if (playFieldView.getCell(0, 2) == playFieldView.getCell(1, 1) &&
                    playFieldView.getCell(0, 2) == playFieldView.getCell(2, 0)) {
                playFieldActivity.win(playFieldView.getCell(0, 2));
                gameEnded = true;
            }
        }
    }

    @Override
    public void onFieldTap(int x, int y) {
        if (!gameEnded) {
            if (playFieldView.getCell(x, y) == Symbol.NOTHING && currTurn == mySymbol) {
                playFieldView.setCell(x, y, mySymbol);
                checkToWin(x, y);
                turns++;
                if (!gameEnded) {
                    if (turns < 9) {
                        int[] move = bot.move(playFieldView.matrix);
                        playFieldView.setCell(move[1], move[0], botSymbol);
                        checkToWin(move[1], move[0]);
                        turns++;
                        if (turns == 9) {
                            Toast.makeText(playFieldActivity, "Draw", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(playFieldActivity, "Draw", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }
}
