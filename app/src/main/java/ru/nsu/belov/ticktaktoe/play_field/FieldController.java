package ru.nsu.belov.ticktaktoe.play_field;

import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.Iterator;
import java.util.Random;

import ru.nsu.belov.ticktaktoe.PlayFieldActivity;

/**
 * Created by Evgeny on 13.12.2017.
 */

public class FieldController extends Thread implements OnFieldTapListener {
    private final FirebaseDatabase database;
    private boolean searchingWaitingGames = false;
    private DataSnapshot lastWaitingGamesSnapshot = null;
    private Iterator<DataSnapshot> waitingGamesIterator;
    private PlayFieldView playFieldView;
    private PlayFieldActivity playFieldActivity;
    private DatabaseReference gameRef;
    private Symbol currTurn;
    private String gameToken;
    private Symbol mySymbol;
    private ValueEventListener gameListChanged;
    private ValueEventListener waitForStartEventListener;
    private ValueEventListener closeEventListener;
    private boolean gameEnded = false;
    private boolean gameHost;


    public FieldController(PlayFieldView playFieldView, PlayFieldActivity playFieldActivity) {
        database = FirebaseDatabase.getInstance();
        this.playFieldView = playFieldView;
        currTurn = Symbol.CROSS;
        this.playFieldActivity = playFieldActivity;
    }


    public void addNewGame() {
        gameHost = true;
        gameRef = database.getReference("waiting_games").push();
        gameToken = gameRef.getKey();
        gameRef.child("opponent").setValue("empty");
        gameRef = database.getReference("active_games").child(gameToken);
        gameRef.child("turn").setValue(Symbol.CROSS);
        Random random = new Random();
        if (random.nextBoolean()) {
            mySymbol = Symbol.CROSS;
        } else {
            mySymbol = Symbol.CIRCLE;
        }
        gameRef.child("opponent_symbol").setValue((mySymbol == Symbol.CIRCLE) ? Symbol.CROSS : Symbol.CIRCLE);
        playFieldActivity.setCurrentTurn(currTurn);
        playFieldActivity.setYourSymbolTextView(mySymbol);
        waitForStartEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Boolean start = dataSnapshot.child("start").getValue(Boolean.class);
                if (start != null && start == true) {
                    subscribeToFieldUpdate();
                    playFieldActivity.setProgressBarVisible(false);
                    gameRef.removeEventListener(waitForStartEventListener);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(playFieldView.getContext(), databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
        gameRef.addValueEventListener(waitForStartEventListener);

//        closeEventListener = new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                Boolean start = dataSnapshot.child("start").getValue(Boolean.class);
//                if (start != null && start == true) {
//                    subscribeToFieldUpdate();
//                    playFieldActivity.setProgressBarVisible(false);
//                    gameRef.removeEventListener(waitForStartEventListener);
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                Toast.makeText(playFieldView.getContext(), databaseError.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        };
//        database.getReference("active_games").child(gameToken).child("opponent_leave").addValueEventListener()
    }

    public void onClose() {
        if (gameHost) {
            database.getReference("waiting_games").child(gameToken).removeValue();
        } else {
            if(gameListChanged!=null)
            database.getReference("waiting_games").removeEventListener(gameListChanged);
        }
        if (!gameEnded && gameToken != null) {
            database.getReference("active_games").child(gameToken).child("opponent_leave").setValue(true);
        }
    }

    private void startGame() {
        gameRef.removeValue();
        gameRef = database.getReference("active_games").child(gameToken);
        gameRef.child("opponent_symbol").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue(String.class).equals(Symbol.CIRCLE.name())) {
                    mySymbol = Symbol.CIRCLE;
                } else {
                    mySymbol = Symbol.CROSS;
                }
                playFieldActivity.setCurrentTurn(currTurn);
                playFieldActivity.setYourSymbolTextView(mySymbol);
                gameRef.child("start").setValue(true);
                subscribeToFieldUpdate();
                playFieldActivity.setProgressBarVisible(false);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(playFieldView.getContext(), databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void subscribeToFieldUpdate() {
        database.getReference("active_games").child(gameToken).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                String[] strings = dataSnapshot.getKey().split("x");
                if (strings.length == 2) {
                    Symbol s = dataSnapshot.getValue(Symbol.class);
                    if (currTurn == Symbol.CROSS) {
                        currTurn = Symbol.CIRCLE;
                    } else {
                        currTurn = Symbol.CROSS;
                    }
                    playFieldActivity.setCurrentTurn(currTurn);
                    int x = Integer.valueOf(strings[0]);
                    int y = Integer.valueOf(strings[1]);
                    playFieldView.setCell(x, y, s);
                    checkToWin(x, y);
                } else if (dataSnapshot.getKey().equals("opponent_leave")) {
                    Toast.makeText(playFieldActivity, "Opponent leave the game", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void checkToWin(int x, int y) {
        if (playFieldView.getCell(0, y) == playFieldView.getCell(1, y) &&
                playFieldView.getCell(0, y) == playFieldView.getCell(2, y)) {
            playFieldActivity.win(playFieldView.getCell(0, y));
            gameEnded = true;
            return;
        }
        if (playFieldView.getCell(x, 0) == playFieldView.getCell(x, 1) &&
                playFieldView.getCell(x, 0) == playFieldView.getCell(x, 2)) {
            playFieldActivity.win(playFieldView.getCell(x, 0));
            gameEnded = true;
            return;
        }
        if (x == y) {
            if (playFieldView.getCell(0, 0) == playFieldView.getCell(1, 1) &&
                    playFieldView.getCell(0, 0) == playFieldView.getCell(2, 2)) {
                playFieldActivity.win(playFieldView.getCell(0, 0));
                gameEnded = true;
            }
        } else if (x + y == 2) {
            if (playFieldView.getCell(0, 2) == playFieldView.getCell(1, 1) &&
                    playFieldView.getCell(0, 2) == playFieldView.getCell(2, 0)) {
                playFieldActivity.win(playFieldView.getCell(0, 2));
                gameEnded = true;
            }
        }
    }

    private void foundGame() {
        if (waitingGamesIterator.hasNext()) {
            DataSnapshot dataSnapshot = waitingGamesIterator.next();
            final DatabaseReference gameReference = dataSnapshot.getRef();
            gameReference.runTransaction(new Transaction.Handler() {
                @Override
                public Transaction.Result doTransaction(MutableData mutableData) {
                    if (mutableData.child("opponent") == null) {
                        return Transaction.abort();
                    }
                    String opponent = mutableData.child("opponent").getValue(String.class);
                    if ("empty".equals(opponent)) {
                        mutableData.child("opponent").setValue(FirebaseAuth.getInstance().getCurrentUser().getUid());
                        gameRef = gameReference;
                        gameToken = gameRef.getKey();
                        database.getReference("waiting_games").removeEventListener(gameListChanged);
                        return Transaction.success(mutableData);
                    }
                    return Transaction.abort();
                }

                @Override
                public void onComplete(DatabaseError databaseError, boolean committed, DataSnapshot dataSnapshot) {
                    if (committed) {
                        startGame();
                    } else {
                        foundGame();
                    }
                }


            });
        } else if (lastWaitingGamesSnapshot != null) {
            waitingGamesIterator = lastWaitingGamesSnapshot.getChildren().iterator();
            lastWaitingGamesSnapshot = null;
            foundGame();
        } else {
            searchingWaitingGames = false;
        }

    }


    public void connect() {
        gameHost = false;
        gameListChanged = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lastWaitingGamesSnapshot = dataSnapshot;
                if (!searchingWaitingGames) {
                    Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                    waitingGamesIterator = children.iterator();
                    if (waitingGamesIterator.hasNext()) {
                        lastWaitingGamesSnapshot = null;
                        searchingWaitingGames = true;
                        foundGame();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(playFieldView.getContext(), databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
        database.getReference("waiting_games").addValueEventListener(gameListChanged);
    }


    @Override
    public void onFieldTap(final int x, final int y) {
        if (!gameEnded) {
            if (playFieldView.getCell(x, y) == Symbol.NOTHING) {
                final DatabaseReference myRef = database.getReference("active_games").child(gameToken);
                myRef.child("turn").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Symbol turn = dataSnapshot.getValue(Symbol.class);
                        if (turn == mySymbol) {
                            myRef.child("turn").setValue((mySymbol == Symbol.CIRCLE) ? Symbol.CROSS : Symbol.CIRCLE);
//                        playFieldView.setCell(x, y, mySymbol);
                            myRef.child(x + "x" + y).setValue(mySymbol);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(playFieldView.getContext(), databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }
}
