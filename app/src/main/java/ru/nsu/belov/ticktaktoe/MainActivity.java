package ru.nsu.belov.ticktaktoe;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;

import ru.nsu.belov.ticktaktoe.authentication.User;

public class MainActivity extends AppCompatActivity {
    private Button newGameButton;
    private Button connectButton;
    private Button botButton;

    public static Intent getCreateIntent(Context context, User user) {
        Intent intent = new Intent(context, MainActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        newGameButton = findViewById(R.id.new_game_btn);
        connectButton = findViewById(R.id.connect_btn);
        botButton = findViewById(R.id.bot_btn);
        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String type;
                if (view == newGameButton) {
                    type = "new_game";
                } else if (view == connectButton) {
                    type = "connect";
                } else {
                    type = "bot";
                }
                startActivity(PlayFieldActivity.getCreateIntent(MainActivity.this, type));
            }
        };
        newGameButton.setOnClickListener(clickListener);
        connectButton.setOnClickListener(clickListener);
        botButton.setOnClickListener(clickListener);
        findViewById(R.id.logout_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(MainActivity.this,LoginActivity.class));
                finish();

            }
        });
    }
}

