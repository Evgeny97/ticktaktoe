package ru.nsu.belov.ticktaktoe.authentication;

import java.io.Serializable;

/**
 * Created by Evgeny on 15.12.2017.
 */

public class User implements Serializable {
    private String login;
    private String token;

    User(String login, String token) {
        this.login = login;
        this.token = token;
    }

    public String getLogin() {
        return login;
    }

    public String getToken() {
        return token;
    }
}
