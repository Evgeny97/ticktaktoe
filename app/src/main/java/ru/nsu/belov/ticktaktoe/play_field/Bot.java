package ru.nsu.belov.ticktaktoe.play_field;

import java.util.Arrays;

/**
 * Created by Evgeny on 20.12.2017.
 */

public class Bot {
    private Symbol[] tmpMatrix;
    private Symbol botSymbol;
    private Symbol playerSymbol;

    public Bot(Symbol botSymbol) {
        this.botSymbol = botSymbol;
        playerSymbol = (botSymbol == Symbol.CIRCLE) ? Symbol.CROSS : Symbol.CIRCLE;
    }

    /**
     * Get next best move for computer. Return int[2] of {row, col}
     */

    int[] move(Symbol[] matrix) {
        tmpMatrix = Arrays.copyOf(matrix, matrix.length);
        int[] result = minimax(2, false); // depth, max turn
        return new int[]{result[1], result[2]};   // row, col
    }

    /**
     * Recursive minimax at level of depth for either maximizing or minimizing player.
     * Return int[3] of {score, row, col}
     */
    private int[] minimax(int depth, boolean isPlayer) {
        int possibleMoves = 0;
        for (Symbol s : tmpMatrix) {
            if (s == Symbol.NOTHING) {
                possibleMoves++;
            }
        }

        // mySeed is maximizing; while oppSeed is minimizing
        int bestScore = (!isPlayer) ? Integer.MIN_VALUE : Integer.MAX_VALUE;
        int currentScore;
        int bestRow = -1;
        int bestCol = -1;

        if (possibleMoves == 0 || depth == 0) {
            bestScore = evaluate();
        } else {
            for (int i = 0, tmpMatrixLength = tmpMatrix.length; i < tmpMatrixLength; i++) {
                if (tmpMatrix[i] == Symbol.NOTHING) {
                    tmpMatrix[i] = (isPlayer) ? playerSymbol : botSymbol;
                    currentScore = minimax(depth - 1, !isPlayer)[0];
                    if (!isPlayer) {  // mySeed (computer) is maximizing player
                        if (currentScore > bestScore) {
                            bestScore = currentScore;
                            bestRow = (int) i / 3;
                            bestCol = i % 3;
                        }
                    } else {  // oppSeed is minimizing player
                        if (currentScore < bestScore) {
                            bestScore = currentScore;
                            bestRow = (int) i / 3;
                            bestCol = i % 3;
                        }
                    }
                    tmpMatrix[i] = Symbol.NOTHING;
                }
            }
        }
        return new int[]{bestScore, bestRow, bestCol};
    }


    /**
     * The heuristic evaluation function for the current board
     *
     * @Return +100, +10, +1 for EACH 3-, 2-, 1-in-a-line for computer.
     * -100, -10, -1 for EACH 3-, 2-, 1-in-a-line for opponent.
     * 0 otherwise
     */

    private int evaluate() {
        int score = 0;
        // Evaluate score for each of the 8 lines (3 rows, 3 columns, 2 diagonals)
        score += evaluateLine(0, 0, 0, 1, 0, 2);  // row 0
        score += evaluateLine(1, 0, 1, 1, 1, 2);  // row 1
        score += evaluateLine(2, 0, 2, 1, 2, 2);  // row 2
        score += evaluateLine(0, 0, 1, 0, 2, 0);  // col 0
        score += evaluateLine(0, 1, 1, 1, 2, 1);  // col 1
        score += evaluateLine(0, 2, 1, 2, 2, 2);  // col 2
        score += evaluateLine(0, 0, 1, 1, 2, 2);  // diagonal
        score += evaluateLine(0, 2, 1, 1, 2, 0);  // alternate diagonal
        return score;
    }

    /**
     * The heuristic evaluation function for the given line of 3 cells
     *
     * @Return +100, +10, +1 for 3-, 2-, 1-in-a-line for computer.
     * -100, -10, -1 for 3-, 2-, 1-in-a-line for opponent.
     * 0 otherwise
     */
    private int evaluateLine(int row1, int col1, int row2, int col2, int row3, int col3) {
        int score = 0;

        // First cell

        if (tmpMatrix[row1 * 3 + col1] == botSymbol) {
            score = 1;
        } else if (tmpMatrix[row1 * 3 + col1] == playerSymbol) {
            score = -1;
        }
        // Second cell
        if (tmpMatrix[row2 * 3 + col2] == botSymbol) {
            if (score == 1) {   // cell1 is mySeed
                score = 10;
            } else if (score == -1) {  // cell1 is oppSeed
                return 0;
            } else {  // cell1 is empty
                score = 1;
            }
        } else if (tmpMatrix[row2 * 3 + col2] == playerSymbol) {
            if (score == -1) { // cell1 is oppSeed
                score = -10;
            } else if (score == 1) { // cell1 is mySeed
                return 0;
            } else {  // cell1 is empty
                score = -1;
            }
        }

        // Third cell
        if (tmpMatrix[row3 * 3 + col3] == botSymbol) {
            if (score > 0) {  // cell1 and/or cell2 is mySeed
                score *= 10;
            } else if (score < 0) {  // cell1 and/or cell2 is oppSeed
                return 0;
            } else {  // cell1 and cell2 are empty
                score = 1;
            }
        } else if (tmpMatrix[row3 * 3 + col3] == playerSymbol) {
            if (score < 0) {  // cell1 and/or cell2 is oppSeed
                score *= 10;
            } else if (score > 1) {  // cell1 and/or cell2 is mySeed
                return 0;
            } else {  // cell1 and cell2 are empty
                score = -1;
            }
        }
        return score;
    }
}
