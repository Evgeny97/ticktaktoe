package ru.nsu.belov.ticktaktoe;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import ru.nsu.belov.ticktaktoe.play_field.BotFieldController;
import ru.nsu.belov.ticktaktoe.play_field.FieldController;
import ru.nsu.belov.ticktaktoe.play_field.PlayFieldView;
import ru.nsu.belov.ticktaktoe.play_field.Symbol;

public class PlayFieldActivity extends AppCompatActivity {
    static final String GAME_TYPE_TAG = "GAME_TYPE";
    BotFieldController botFieldController;
    private PlayFieldView playFieldView;
    private ProgressBar progressBarView;
    private TextView curTurnTextView;
    private TextView winTextView;
    private TextView yourSymbolTextView;
    private FieldController fieldController;
    private LinearLayout playFieldLinearLayout;

    public static Intent getCreateIntent(Context context, String gameType) {
        Intent intent = new Intent(context, PlayFieldActivity.class);
        intent.putExtra(GAME_TYPE_TAG, gameType);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_field);
        playFieldView = findViewById(R.id.play_field);
        progressBarView = findViewById(R.id.progress_bar);
        curTurnTextView = findViewById(R.id.current_turn);
        winTextView = findViewById(R.id.win_text);
        yourSymbolTextView = findViewById(R.id.your_symbol);
        playFieldLinearLayout = findViewById(R.id.play_field_linear_layout);

        String stringExtra = getIntent().getStringExtra(GAME_TYPE_TAG);
        if (stringExtra.equals("bot")) {
            botFieldController = new BotFieldController(playFieldView, this);
        } else {
            fieldController = new FieldController(playFieldView, this);
            if (stringExtra.equals("new_game")) {
                fieldController.addNewGame();
            } else {
                fieldController.connect();
            }
        }
    }

    public void setCurrentTurn(Symbol symbol) {
        curTurnTextView.setText("Current turn: " + symbol.name());
    }

    public void setYourSymbolTextView(Symbol symbol) {
        yourSymbolTextView.setText("Your symbol: " + symbol.name());
    }

    public void win(Symbol symbol) {
        winTextView.setText("Game over. The winner is " + symbol.name());
        winTextView.setVisibility(View.VISIBLE);
        progressBarView.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        String stringExtra = getIntent().getStringExtra(GAME_TYPE_TAG);
        if (!stringExtra.equals("bot")) {
            fieldController.onClose();
        }
    }

    public void setProgressBarVisible(boolean visible) {
        if (visible) {
            playFieldLinearLayout.setVisibility(View.GONE);
            progressBarView.setVisibility(View.VISIBLE);
        } else {
            playFieldLinearLayout.setVisibility(View.VISIBLE);
            progressBarView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        String stringExtra = getIntent().getStringExtra(GAME_TYPE_TAG);
        if (stringExtra.equals("bot")) {
            playFieldView.setOnFieldTapListener(botFieldController);
        } else {
            playFieldView.setOnFieldTapListener(fieldController);
        }
    }
}
