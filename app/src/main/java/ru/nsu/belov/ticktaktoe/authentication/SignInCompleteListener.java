package ru.nsu.belov.ticktaktoe.authentication;

/**
 * Created by Evgeny on 15.12.2017.
 */

public interface SignInCompleteListener {
    void onSignInSuccess(User user);
    void onSignInFailure(Throwable exception);
}
