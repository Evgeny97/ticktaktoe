package ru.nsu.belov.ticktaktoe.authentication;

/**
 * Created by Evgeny on 15.12.2017.
 */

public interface AuthenticationController {
    void signInWithLoginAndPassword(String login, String password, SignInCompleteListener signInCompleteListener);

    void createNewUser(String login, String password, CreateUserCompleteListener createUserCompleteListener);
}
