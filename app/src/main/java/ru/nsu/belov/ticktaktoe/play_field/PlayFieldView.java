package ru.nsu.belov.ticktaktoe.play_field;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import ru.nsu.belov.ticktaktoe.R;

/**
 * Created by Evgeny on 10.12.2017.
 */

public class PlayFieldView extends View implements View.OnTouchListener {
    Bitmap fieldBitmap;
    Bitmap crossBitmap;
    Bitmap circleBitmap;
    Symbol[] matrix;
    private int fieldSize = 0;
    OnFieldTapListener onFieldTapListener = null;
    private boolean sized = false;

    public PlayFieldView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
        this.setOnTouchListener(this);
    }


    public Symbol getCell(int x, int y) {
        return matrix[y * 3 + x];
    }

    public void setOnFieldTapListener(OnFieldTapListener onFieldTapListener) {
        this.onFieldTapListener = onFieldTapListener;
    }

    private void init() {
        matrix = new Symbol[9];
        for (int i = 0; i < 9; i++) {
            matrix[i] = Symbol.NOTHING;
        }
   /*     crossPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        crossPaint.setColor(Color.RED);
        zPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        zPaint.setColor(Color.BLUE);
        fieldPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        fieldPaint.setColor(Color.GRAY);*/
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int desiredWidth = 1000;
        int desiredHeight = 1000;

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            width = Math.min(desiredWidth, widthSize);
        } else {
            width = desiredWidth;
        }

        if (heightMode == MeasureSpec.EXACTLY) {
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            height = Math.min(desiredHeight, heightSize);
        } else {
            height = desiredHeight;
        }
        int minSize = Math.min(width, height);
        if (!sized) {
            sized = true;
            resizeImages(minSize);
            fieldSize = minSize;
        }

        setMeasuredDimension(minSize, minSize);
    }

    private void resizeImages(int i) {
        Bitmap tmpBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ttt_field);
        fieldBitmap = Bitmap.createScaledBitmap(tmpBitmap, i,
                i, false);
        tmpBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.cross1);
        crossBitmap = Bitmap.createScaledBitmap(tmpBitmap, i / 3,
                i / 3, false);
        tmpBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.circle);
        circleBitmap = Bitmap.createScaledBitmap(tmpBitmap, i / 3,
                i / 3, false);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(fieldBitmap, 0, 0, null);
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                if (matrix[y * 3 + x] == Symbol.CROSS) {
                    canvas.drawBitmap(crossBitmap, getWidth() / 3 * x, getHeight() / 3 * y, null);
                } else if (matrix[y * 3 + x] == Symbol.CIRCLE) {
                    canvas.drawBitmap(circleBitmap, getWidth() / 3 * x, getHeight() / 3 * y, null);
                }
            }
        }
    }


    @Override
    public boolean performClick() {
        return super.performClick();
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            if (onFieldTapListener != null) {
                int x = (int) motionEvent.getX() / (fieldSize/3);
                int y = (int) motionEvent.getY() / (fieldSize/3);
                onFieldTapListener.onFieldTap(x, y);
            }
        }
        return true;
    }

    public void setCell(int x, int y, Symbol symbol) {
        matrix[y * 3 + x] = symbol;
        this.postInvalidate();
    }
}
